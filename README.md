# Requirements

## specially built packages
opencv (built with -fPIC and as a static library)

## pkg-config packages (provided externally)
lcm eigen3

## pkg-config package (provided on bitbucket)
lcmtypes_perllcm

## libraries

## misc packages

Installing Locally
=================

Make a build directory in the top-level directory of this project and type 'make'.  The project will be installed to build.

Installing System-wide
======================

Type 'make BUILD_PREFIX=/usr/local'.  You will need root priviledges.

